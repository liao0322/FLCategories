//
//  NSString+Extension.h
//  BuyVegetablesTreasure
//
//  Created by DamonLiao on 2017/1/13.
//  Copyright © 2017年 c521xiong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

- (NSString *)clearAllSpace;
- (NSString *)clearLeadAndTailSpace;
- (NSString *)decimalNumber;
+ (NSString *)moneyStringWithString:(NSString *)string;

+ (NSString *)decimalNumberWithDouble:(double)conversionValue;

/// 带货币符号的金额格式，小数位最多4，最少2，带千位符
+ (NSString *)currencyStringWithDoubleValue:(double)doubleValue;
/// 不带货币符号的金额格式，小数位最多4，最少2，带千位符
+ (NSString *)decimalStringWithDoubleValue:(double)doubleValue;

/// 阿拉伯数字金额转中文繁体
+ (NSString *)traditionalMoneyStringWithString:(NSString*)string;

/// 统计 某个字符在字符串中出现的次数
- (NSInteger)countOccurencesOfString:(NSString *)searchString;

/// 在宽度既定的情况下，计算文本的高度
+ (CGFloat)stringHeightWithString:(NSString *)string maxWidth:(CGFloat)width font:(UIFont *)font;

@end
