//
//  FLViewController.m
//  FLCategories
//
//  Created by liao0322 on 03/15/2018.
//  Copyright (c) 2018 liao0322. All rights reserved.
//

#import "FLViewController.h"


@interface FLViewController ()

@end

@implementation FLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSInteger v = 9;
    NSString *letter = [self letterWithNumber:v];
    NSLog(@"%@", letter);
}

- (NSInteger)fibonacciValueWithNumber:(NSInteger)number {
    switch (number) {
        case 1:
            return 1;
            break;
        case 3:
            return 2;
            break;
        case 5:
            return 3;
            break;
        case 7:
            return 5;
            break;
        case 9:
            return 8;
            break;
        default:
            return 0;
            break;
    }
}

- (NSString *)letterWithNumber:(NSInteger)number {
    NSInteger fibonacciValue = [self fibonacciValueWithNumber:number];
    fibonacciValue += 96;
    return [NSString stringWithFormat:@"%c", (int)fibonacciValue];
}







@end
