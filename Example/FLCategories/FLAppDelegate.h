//
//  FLAppDelegate.h
//  FLCategories
//
//  Created by liao0322 on 03/15/2018.
//  Copyright (c) 2018 liao0322. All rights reserved.
//

@import UIKit;

@interface FLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
